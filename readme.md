
## [Generative art](https://en.wikipedia.org/wiki/Generative_art)  

- https://fronkonstin.com/  
- https://www.artnome.com/news/2018/8/8/why-love-generative-art  

### in R

- [Katharina Brunner](https://github.com/cutterkom/generativeart)  
- [Thomas Lin Pedersen - data-imaginist](https://www.data-imaginist.com/art) - just inspiration no code here, but Thomas hase several relevant packages  
- [Danielle Navarro](https://art.djnavarro.net/) - inspiration. And also code:
  - [jasmines](https://jasmines.djnavarro.net/)  
  - [rosemary](https://github.com/djnavarro/rosemary)  
- [Marcus Volz](https://github.com/marcusvolz/mathart)  

- [#rtistry](https://twitter.com/hashtag/rtistry?src=hashtag_click)  
- [#genuary2021](https://twitter.com/hashtag/genuary2021?src=hashtag_click)  

### slightly off topic

- https://github.com/ryantimpe/rstereogram  

### printing

All **A series** papers hav aspect `sqrt(2)` so e.g. if A4 is 210mm wide, the it is cca 1.414*210mm high. So if A series is the format for print, but we have only pixels as resolution we can use same aspect. For example 5000px landscape wide image should be 1/sqrt(2)*5000 high.  

On Ubuntu to get firefox window bigger, than screen resolution go:

`
firefox -new-window ferns.html
sleep 5
wid=`wmctrl -l | grep Firefox | grep "ferns" | cut -d " " -f 1`
wmctrl -i -r $wid -e 0,0,0,5000,1/1.414214*5000
`

The in the Firefox window right-click --> Take Screenshot --> manually select whole area of the page --> Download