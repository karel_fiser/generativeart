---
output: 
  html_document:
    theme: simple
    highlight: tango
---

```{r setup, echo=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE, cache = FALSE)
# Note: https://stackoverflow.com/questions/31753897/2-column-section-in-r-markdown

# For printing I opened in Firefox and in the console run:
# :screenshot --selector ".main-container" --dpr 2.025
# The resulting image was then centered and scaled via Inkscape 
# To sit at a center of A2 and have width 480mm
```


```{css, echo=FALSE}
body .main-container {
  max-width: 100% !important;
  width: 100% !important;
  max-height: 100% !important;
  height:100% !important;
  overflow:hidden;
  margin:10px;
  padding:0;
}
body {
  width:2000px !important;
  max-width: 2000px !important;
  max-height: 1600px !important;
  height:1600px !important;
  color:black;
  background-color:white;
}

h3 {
  color:black;
}
```



```{r main_plot, dpi=600, out.width='100%', results='asis'}
knitr::include_graphics("floating_point2.png")
```

```{r, include = FALSE}
lines <- readLines("floating_point2.R")
break_line <- 26
col_break_width <- 0.5
```

:::::: {.columns}

::: {.column width="20%"}
### IEEE 754

Floating point numerical computation is notoriously tricky to get
right. The colors of the image are based on the differences between computing

$$
\sin (x - y)
$$

and

$$
\sin x \cos y  - \cos x \sin y
$$

for varying $x$ and $y$ (polar coordinates of the center of the patch).
The difference is analytically always $0$, but in floating point can fluctuate noticeably around that value.

:::
::: {.column width="`r col_break_width`%"}
:::
::: {.column width="`r 40 - col_break_width`%"}

```{r results ="asis"}
cat("```r\n")
cat(lines[1:break_line], sep = "\n")
cat("```\n")
```

:::
::: {.column width="`r col_break_width`%"}
:::
::: {.column width="`r 40 - col_break_width`%"}
```{r results ="asis"}
cat("```r\n")
cat(lines[(break_line + 1):length(lines)], sep = "\n")
cat("```\n")
```

:::
::::::




